;************************************************************************
;* interpolates noaa data from monotic lon/lat gird to lon2d/lat2d grid *
;************************************************************************

;*** Settings ***
domain = "02"
;****************

begin
; open any wrf file wth lat lon information
  wrf_latlon_file  = addfile("/data/Marco/wrfinput/xlat_xlong_info/wrfinput_d" +domain +"_2010_10", "r    ")
  lat2d = wrf_latlon_file->XLAT(0, :, :)
  lon2d = wrf_latlon_file->XLONG(0, :, :)
  landmask = wrf_latlon_file->LANDMASK(0, :, :)
  landmask = where(landmask.eq. 0, -1, landmask); 0 ->-1
  landmask = where(landmask.eq. 1,  0, landmask); 1 -> 0
  landmask = where(landmask.eq.-1,  1, landmask); -1 -> 1

; read noaa file
  noaa_file = addfile("/data/Marco/process_sst/wkmean.1981-present.nc", "r")
  sst_noaa = noaa_file->sst
  time_bnds = noaa_file->time_bnds  

; rgrid2rcm requires a monotonic grid: -90 to 90 N and -180 to 180 E -> reor    der
  sst_noaa&lon(180:359) = sst_noaa&lon(180:359) -360.
  part1 = floattoint(fspan(180, 359, 180))
  part2 = floattoint(fspan(0, 179, 180))
  lon_indices = array_append_record(part1, part2, 0)

  tmp = sst_noaa(:, ::-1, lon_indices)

; finally do regridding
  sst_noaa_rgrid  = (rgrid2rcm(tmp&lat, tmp&lon, tmp, lat2d, lon2d, 0) *sst_noaa@scale_factor) +273.15; apply scale factor and cenvert to Kelvin
  delete(tmp)

; Name dimensions
  sst_noaa_rgrid!0 = "time"
;  sst_noaa_rgrid&time = sst_noaa&time
  sst_noaa_rgrid!1 = "lat"
  sst_noaa_rgrid!2 = "lon"

  loopsizes = dimsizes(sst_noaa_rgrid)
; fix missing values at +- 180 degrees east
  do i=0, loopsizes(1) -1
    do j=0, loopsizes(2) -1
      if ismissing(sst_noaa_rgrid(0, i, j))
        sst_noaa_rgrid(:, i, j) = dim_avg_n(sst_noaa_rgrid(:, i, j-1:j+1), (/1/)); missing values replaced by mean of surrounding lon grid points
      end if
   end do
  end do

  do k=0, loopsizes(0) -1
    sst_noaa_rgrid(k, :, :) = sst_noaa_rgrid(k, :, :) *landmask   
  end do  
 
; write data to file
  noaa_grd_file = addfile("/data/Marco/process_sst/d" +domain +"_rgrid_wkmean.1981-present.nc","c")
  noaa_grd_file->sst = sst_noaa_rgrid
  noaa_grd_file->time_bnds = time_bnds
end
