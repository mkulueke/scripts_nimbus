;*******************************************************************
;* opens every WRF file in a loop and replace era-i sst by noaa sst*
;*******************************************************************
begin
   ; open 6hly rgrid file
  noaa_6hly_rgrid_file = addfile("/data/Marco/process_sst/6hly_rgrid_wkmean.2010-2011.nc","r")
  noaa_sst = noaa_6hly_rgrid_file->sst

  wrf_dates = (/"2010_10", "2010_11", "2010_12", "2011_01", "2011_02", "2011_02", "2011_03", "2011_04", "2011_05"/)

  do i=0, dimsizes(wrf_dates) -1
 
; select corresponding time period from NOAA file
    wrf_date_char = stringtochar(wrf_dates(i))
    year = stringtoint(charactertostring(wrf_date_char(0:3)))
    month = stringtoint(charactertostring(wrf_date_char(5:6)))
    print("Month:")
    print(year +" " +month)
    start_time = cd_inv_calendar(year, month, 01, 00, 00, 00, noaa_sst&time@units, 0)
    if month.eq.12 then
      end_time = cd_inv_calendar(year +1, 01, 01, 00, 00, 00, noaa_sst&time@units, 0)
    else
      end_time = cd_inv_calendar(year, month +1, 01, 00, 00, 00, noaa_sst&time@units, 0)
    end if
      

    noaa_sst_month = noaa_sst({start_time:end_time}, :,: )

; write in wrf file
    wrf_file = addfile("/data/Marco/wrfinput/noaa_sst_for_wrf/noaa_sst_wrflowinp_d01_" +wrf_dates(i), "w")
    wrf_sst = wrf_file->SST
    
    print("Wrf Dimsizes:")
    print(dimsizes(wrf_sst))
    print("NOAA Dimsizes:")
    print(dimsizes(noaa_sst_month))
    wrf_noaa_sst = wrf_sst
    wrf_noaa_sst = (/noaa_sst_month/)

; overwrite sst in wrf file
    wrf_file->SST = wrf_noaa_sst
    delete([/wrf_date_char, year, month, start_time, end_time, noaa_sst_month, wrf_sst, wrf_file, wrf_noaa_sst/])

  end do

end
