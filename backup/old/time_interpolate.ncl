begin
  noaa_file = addfile("/data/Marco/process_sst/wkmean.1981-present.nc", "r")
  sst = noaa_file->sst
  
  ; interpolate to daily
  max_timeidx = dimsizes(sst&time) -1  
  time_6hly = ispan(toint(sst&time(0)*4), toint(sst&time(max_timeidx)*4), 1)/4.
  print(time_6hly)
  print(sst&time(0))
  print(sst&time(max_timeidx))
  
  sst_reorder = sst(lat|:,lon|:,time|:) 
  
  sst_6hly = linint1(sst&time, sst_reorder, False,time_6hly, 0)
  print(dimsizes(sst_6hly))
end
