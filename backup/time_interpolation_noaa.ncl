;****************************************************************************************
;* opens the regridded file and linarly interpolates the time from biweekly to 6 hourly *
;****************************************************************************************
begin
; read regridded  noaa file
  noaa_rgrid_file = addfile("/data/Marco/process_sst/rgrid_wkmean.1981-present.nc", "r")
  sst_noaa_rgrid = noaa_rgrid_file->sst
  sst_noaa_rgrid_time_bnds = noaa_rgrid_file->time_bnds
  sst_noaa_rgrid&time = (/sst_noaa_rgrid_time_bnds(:,1)/)

; interpolate to daily
  start_time = cd_inv_calendar(2010, 9, 01, 00, 00, 00, sst_noaa_rgrid&time@units, 0)
  end_time = cd_inv_calendar(2011, 7, 01, 00, 00, 00, sst_noaa_rgrid&time@units, 0)
  time_6hly = ispan(toint(start_time*4), toint(end_time*4), 1)/4.; ispan o    nly allows integer -> *4 and later /4  
  sst_noaa_rgrid_extract = sst_noaa_rgrid({start_time:end_time}, :, :)
  tmp = sst_noaa_rgrid_extract(lat|:, lon|:, time|:)
  sst_noaa_rgrid_6hly = linint1(tmp&time, tmp, False, time_6hly, 0)
  delete(tmp)
  
  sst_noaa_rgrid_6hly!0 = "lat"
  sst_noaa_rgrid_6hly!1 = "lon"
  sst_noaa_rgrid_6hly!2 = "time"
  tmp = sst_noaa_rgrid_6hly(time|:, lat|:, lon|:)
  tmp&time = time_6hly
  tmp&time@units =  sst_noaa_rgrid&time@units
  ; write data_orig to new file
  noaa_6hly_rgrid_file = addfile("/data/Marco/process_sst/6hly_rgrid_wkmean.2010-2011.nc","c")
  noaa_6hly_rgrid_file->sst = tmp
end
