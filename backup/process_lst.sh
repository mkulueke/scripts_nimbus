#!/bin/bash

# Merge tmax year 1950 to 2017 -> select DJF season -> calculate seasonal mean for DJF -> exclude first and last year
#cdo -selyear,1951/2016 -seasmean -selseason,DJF -selvar,tmax_mask -mergetime /data/BOM_AWAP_GRIDDED/tmax/TRIM/NC/tmax_*.nc /data/Marco/process_lst/daily_lst_DJF_seasmean.nc

# Cut WA
#cdo -sellonlatbox,112,129,-44.5,-10 /data/Marco/process_lst/daily_lst_DJF_seasmean.nc WA_daily_lst_DJF_seasmean.nc

# merge tmax year 1950 to 2017 --> calc monmean -> select only WA
#cdo -sellonlatbox,112,129,-44.5,-10 -monmean -selvar,tmax_mask -mergetime /data/BOM_AWAP_GRIDDED/tmax/TRIM/NC/tmax_*.nc /data/Marco/process_lst/WA_lst_monmean.nc

# calculate 1971-2000 multi-year monthly mean
#cdo -ymonmean -selyear,1971/2000 /data/Marco/process_lst/WA_lst_monmean.nc /data/Marco/process_lst/WA_lst_1971-2000_ymonmean.nc

#------------------------------------------------------------
# merge tmax year 1950 to 2017 --> calc monmean
#cdo -monmean -selvar,tmax_mask -mergetime /data/BOM_AWAP_GRIDDED/tmax/TRIM/NC/tmax_*.nc /data/Marco/process_lst/lst_monmean.nc

# calculate 1971-2000 multi-year monthly mean
#cdo -ymonmean -selyear,1971/2000 /data/Marco/process_lst/lst_monmean.nc /data/Marco/process_lst/lst_1971-2000_ymonmean.nc
