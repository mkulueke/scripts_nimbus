#!/bin/bash

#########################
# First Mask Land Values#
#########################
#cdo -setmissval,-9999 -setvals,0,-9999 /data/NOAA_Reynolds_SST/lsmask.nc temp.nc

#cdo -setmissval,-9999 -mul temp.nc /data/NOAA_Reynolds_SST/sst.mnmean.nc /data/Marco/process_sst/sst.mnmean.masked_land.nc
#rm temp.nc

#cdo -setmissval,-9999 -setvals,0,-9999 /data/NOAA_Reynolds_SST/lsmask.nc temp.nc

#cdo -setmissval,-9999 -mul temp.nc  /data/Marco/process_sst/wkmean.1981-present.nc  /data/Marco/process_sst/wkmean.1981-present_land_masked.nc
#rm temp.nc


##############################
# Fldmean Creates Timeseries #
##############################
# Select box in the Indian Ocean -> calculate field mean -> select season DJF -> calculate seasonal measn for DJF -> exculde first and last year
#cdo -selyear,1983/2017 -seasmean -selseason,DJF, -fldmean -sellonlatbox,106,116,-36,-26 sst.mnmean.masked_land.nc sst_DJF_seasmean_106to116E_26to36S_land_masked.nc


###########################
# Calculate Seasonal Mean #
###########################
# Select box in the Indian Ocean -> select season DJF -> calculate seasonal measn for DJF -> exculde first and last year
#cdo -selyear,1983/2017 -seasmean -selseason,DJF, -sellonlatbox,80,129,-50,-10 /data/Marco/process_sst/sst.mnmean.masked_land.nc /data/Marco/process_sst/sst_DJF_seasmean.nc

# Select box in the Indian Ocean -> select season DJF -> calculate seasonal measn for DJF -> exculde first and last year
#cdo -timmean -selseason,DJF -sellonlatbox,80,129,-50,-10 /data/NOAA_Reynolds_SST/sst.ltm.1971-2000.nc /data/Marco/process_sst/sst_DJF_1971-2000_lt_seasmean.nc

##########################
# Calculate Monthly Mean #
##########################
# Select box in the Indian Ocean
#cdo -sellonlatbox,80,129,-50,-10 /data/Marco/process_sst/sst.mnmean.masked_land.nc /data/Marco/process_sst/IO_sst_monmean.nc

# Select box in the Indian Ocean -> calculate multiyear monmean
#cdo -ymonmean -sellonlatbox,80,129,-50,-10 /data/NOAA_Reynolds_SST/sst.ltm.1971-2000.nc /data/Marco/process_sst/IO_sst_1971-2000_ymonmean.nc
#cdo -ymonmean /data/NOAA_Reynolds_SST/sst.ltm.1971-2000.nc /data/Marco/process_sst/sst_1971-2000_ymonmean.nc

########################
# select La Nina Years #
########################
# strong
#cdo -selyear,1988,1989,1998,1999,2000,2007,2008,2010,2011 /data/Marco/process_sst/rgrid_wkmean.1981-present.nc /data/Marco/process_sst/strong_la_nina_rgrid_wkmean.1981-present.nc

# moderate
#cdo -selyear,1995,1996,2011,2012 /data/Marco/process_sst/rgrid_wkmean.1981-present.nc /data/Marco/process_sst/moderate_la_nina_rgrid_wkmean.1981-present.nc

# weak
#cdo -selyear,1983,1984,1985,2000,2001,2005,2006,2008,2009,2016,2017,2018 /data/Marco/process_sst/rgrid_wkmean.1981-present.nc /data/Marco/process_sst/weak_la_nina_rgrid_wkmean.1981-present.nc

# all
#cdo -selyear,1983,1984,1985,1988,1989,1995,1996,1998,1999,2000,2001,2005,2006,2007,2008,2009,2010,2011,2012,2016,2017,2018 /data/Marco/process_sst/daily_rgrid_wkmean.1981-present.nc /data/Marco/process_sst/all_la_nina_daily_rgrid_wkmean.1981-present.nc
